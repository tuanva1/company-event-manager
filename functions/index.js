const functions = require("firebase-functions");
const admin = require("firebase-admin");
const algoliasearch = require("algoliasearch");

admin.initializeApp(functions.config().firebase);
const algoliaClient = algoliasearch(
  functions.config().algolia.app_id,
  functions.config().algolia.api_key
);
const eventIndex = algoliaClient.initIndex("events");

function exportToAlgolia(event) {
  event.objectID = event.id;
  if (event.attendees === undefined) {
    Object.assign(event, {
      attendees: {}
    });
  }
  eventIndex
    .saveObject(event)
    .then(() => {
      console.log(`Event with id: ${event.id} imported to Algolia`);
    })
    .catch(error => {
      console.error(`Error when import event ${event.id} to Algolia`, error);
    });
  return true;
}

function deleteFromAlgolia(event) {
  eventIndex
    .deleteObject(event.id)
    .then(() => {
      console.log(`Event with id: ${event.id} deleted from Algolia`);
    })
    .catch(error => {
      console.error(`Error when delete event ${event.id} from Algolia`, error);
    });
  return true;
}

exports.addEventToAlgolia = functions.firestore
  .document("events/{eventId}")
  .onCreate(event => exportToAlgolia(event.data.data()));

exports.updateEventToAlgolia = functions.firestore
  .document("events/{eventId}")
  .onUpdate(event => exportToAlgolia(event.data.data()));

exports.deleteEventFromalgolia = functions.firestore
  .document("events/{eventId}")
  .onDelete(event => deleteFromAlgolia(event.data.previous.data()));
