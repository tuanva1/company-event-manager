import mirror, { actions } from "mirrorx";
import eventApi from "../apis/events";
import { isExist } from "../utils/arrays";

const NOT_LOADED_STATE = "not_loaded";
const LOADING_STATE = "loading";
const LOADED_STATE = "loaded";
const ERROR_STATE = "error";

mirror.model({
  name: "app",
  initialState: {
    status: NOT_LOADED_STATE,
    eventIds: [],
    eventById: {},
    keyword: "",
    showFinishedEvent: false
  },
  reducers: {
    setStatus(state, status) {
      return Object.assign({}, state, {
        status
      });
    },

    setEvents(state, events) {
      const eventIds = events.map(e => e.id);
      const eventById = events.reduce(
        (byId, event) => Object.assign({}, byId, { [event.id]: event }),
        {}
      );
      return Object.assign({}, state, {
        eventIds,
        eventById
      });
    },

    setEvent(state, event) {
      let newState = Object.assign({}, state, {
        eventById: {
          ...state.eventById,
          [event.id]: event
        }
      });
      if (!isExist(state.eventIds, event.id, (a1, a2) => a1 === a2)) {
        newState = Object.assign({}, newState, {
          eventIds: [...state.eventIds, event.id]
        });
      }
      return newState;
    },

    removeEventId(state, eventId) {
      return Object.assign({}, state, {
        eventIds: state.eventIds.filter(id => id !== eventId)
      });
    },

    setKeyword(state, keyword) {
      return Object.assign({}, state, {
        keyword
      });
    },

    setShowFinishedEvent(state, checked) {
      return Object.assign({}, state, {
        showFinishedEvent: checked
      });
    }
  },
  effects: {
    async loadEvents() {
      try {
        actions.app.setStatus(LOADING_STATE);
        const events = await eventApi.getEvents();
        actions.app.setEvents(events);
        actions.app.setStatus(LOADED_STATE);
      } catch (error) {
        actions.app.setStatus(ERROR_STATE);
      }
    },

    async searchEvents(action, getState) {
      const state = getState();
      try {
        actions.app.setStatus(LOADING_STATE);
        const events = await eventApi.searchEvents(
          state.app.keyword,
          state.app.showFinishedEvent
        );
        actions.app.setEvents(events);
        actions.app.setStatus(LOADED_STATE);
      } catch (error) {
        actions.app.setStatus(ERROR_STATE);
      }
    },

    async addAttendee(attendee, getState) {
      const state = getState();
      let selectedEvent = state.app.eventById[attendee.eventId];
      try {
        actions.notification.showNotification("Updating...");
        const newAttendee = await eventApi.addAttendee(attendee);
        selectedEvent = Object.assign({}, selectedEvent, {
          attendees: {
            ...selectedEvent.attendees,
            [newAttendee.id]: newAttendee
          }
        });
        actions.app.setEvent(selectedEvent);
        actions.notification.showNotification("Update successed");
      } catch (error) {
        actions.notification.showNotification("Update failed");
      }
    },

    async removeAttendee({ eventId, attendeeId }, getState) {
      const state = getState();
      let selectedEvent = state.app.eventById[eventId];
      try {
        actions.notification.showNotification("Updating...");
        await eventApi.removeAttendee(eventId, attendeeId);
        const {
          [attendeeId]: deletedAttendee,
          ...remmainAtteendee
        } = selectedEvent.attendees;
        selectedEvent = Object.assign({}, selectedEvent, {
          attendees: remmainAtteendee
        });
        actions.app.setEvent(selectedEvent);
        actions.notification.showNotification("Update successed");
      } catch (error) {
        actions.notification.showNotification("Update failed");
      }
    },

    async removeEvent(eventId) {
      try {
        actions.notification.showNotification("Removing...");
        await eventApi.removeEvent(eventId);
        actions.app.removeEventId(eventId);
        actions.notification.showNotification("Remove successed");
      } catch (error) {
        actions.notification.showNotification("Remove failed");
      }
    },

    async saveEvent(event) {
      try {
        actions.notification.showNotification("Updating...");
        await eventApi.saveEvent(event);
        actions.app.setEvent(event);
        actions.notification.showNotification("Update successed");
      } catch (error) {
        actions.notification.showNotification("Update failed");
      }
    }
  }
});
