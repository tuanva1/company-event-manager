import mirror, { actions } from "mirrorx";

let timeoutId = null;

mirror.model({
  name: "notification",
  initialState: {
    message: ""
  },
  reducers: {
    setNotification(state, message) {
      return { message };
    },
    clearNotification(state) {
      return { message: "" };
    }
  },
  effects: {
    showNotification(message) {
      actions.notification.setNotification(message);
      if (timeoutId) clearTimeout(timeoutId);
      timeoutId = setTimeout(
        () => actions.notification.clearNotification(),
        2000
      );
    }
  }
});
