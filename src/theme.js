import { createMuiTheme } from "material-ui";
import teal from "material-ui/colors/teal";
import amber from "material-ui/colors/amber";

export function createTheme() {
  return createMuiTheme({
    palette: {
      primary: teal,
      secondary: amber
    }
  });
}
