import React from "react";
import { render } from "mirrorx";
import { MuiThemeProvider, Reboot } from "material-ui";
import { createTheme } from "./theme";
import "./index.css";
import "./modules/app";
import "./modules/notification";
import NotificationProvider from "./components/NotificationProvider";
import App from "./components/App";

const theme = createTheme();

render(
  <MuiThemeProvider theme={theme}>
    <NotificationProvider>
      <Reboot />
      <App />
    </NotificationProvider>
  </MuiThemeProvider>,
  document.getElementById("root")
);
