import React, { PureComponent } from "react";
import ReactDOM from "react-dom";
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  DialogContentText,
  Button
} from "material-ui";

export default class ConfirmModal extends PureComponent {
  renderModal() {
    const { open, message, onClose, onSubmit } = this.props;
    return (
      <Dialog open={open} onClose={this.onClose} fullWidth>
        <DialogTitle>Confirmation</DialogTitle>
        <DialogContent>
          <DialogContentText>{message}</DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={onClose}>Cancel</Button>
          <Button onClick={onSubmit} color="primary">
            Confirm
          </Button>
        </DialogActions>
      </Dialog>
    );
  }

  render() {
    return ReactDOM.createPortal(
      this.renderModal(),
      document.getElementById("modals")
    );
  }
}
