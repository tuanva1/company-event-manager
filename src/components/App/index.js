import React, { PureComponent } from "react";
import autobind from "class-autobind";
import throttle from "throttle-debounce/throttle";
import moment from "moment";
import { actions, connect } from "mirrorx";
import {
  Grid,
  Button,
  Icon,
  CircularProgress,
  Typography,
  withStyles
} from "material-ui";
import Header from "../Header";
import SearchBox from "../SearchBox";
import EventCard from "../EventCard";
import EditEventModal from "../EditEventModal";

const styles = theme => ({
  mainContent: {
    padding: 5,
    width: "100%",
    maxWidth: 1160,
    margin: "0 auto 60px auto"
  },
  addBtn: {
    position: "fixed",
    bottom: 40,
    right: 40
  }
});

class App extends PureComponent {
  state = {
    openCreateModal: false
  };

  constructor(props) {
    super(props);
    autobind(this);
    this.searchEvents = throttle(300, actions.app.searchEvents);
  }

  componentDidMount() {
    actions.app.loadEvents();
  }

  onCreateEvent(event) {
    actions.app.saveEvent(event);
    this.setState({ openCreateModal: false });
  }

  onKeywordChange(keyword) {
    actions.app.setKeyword(keyword);
    this.searchEvents();
  }

  onToggleShowFinishEvent(checked) {
    actions.app.setShowFinishedEvent(checked);
    this.searchEvents();
  }

  renderEventList() {
    const { events, status } = this.props;
    if (status === "loading") {
      return (
        <Grid item xs={12} style={{ textAlign: "center", marginTop: 20 }}>
          <CircularProgress size={50} />
        </Grid>
      );
    }
    if (status === "error") {
      return (
        <Grid item xs={12} style={{ textAlign: "center", marginTop: 20 }}>
          <Typography>Some error happened. Please refresh the page</Typography>
        </Grid>
      );
    }
    if (status === "loaded" && events.length === 0) {
      return (
        <Grid item xs={12} style={{ textAlign: "center", marginTop: 20 }}>
          <Typography>There is no event to display</Typography>
        </Grid>
      );
    }

    return events.map((e, index) => (
      <Grid key={e.id} item xs={12}>
        <EventCard event={e} />
      </Grid>
    ));
  }

  render() {
    const { classes, keyword, showFinishedEvent } = this.props;
    const { openCreateModal } = this.state;
    return (
      <div>
        <Header />
        <Grid container spacing={24} className={classes.mainContent}>
          <Grid item xs={12}>
            <SearchBox
              keyword={keyword}
              onKeywordChange={this.onKeywordChange}
              showFinishedEvent={showFinishedEvent}
              toggleShowFinishedEvent={this.onToggleShowFinishEvent}
            />
          </Grid>
          {this.renderEventList()}
        </Grid>
        <Button
          fab
          color="secondary"
          className={classes.addBtn}
          onClick={() => this.setState({ openCreateModal: true })}
        >
          <Icon>add</Icon>
        </Button>
        {openCreateModal && (
          <EditEventModal
            open={true}
            onClose={() => this.setState({ openCreateModal: false })}
            onSubmit={this.onCreateEvent}
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  const { eventIds, eventById, keyword, ...other } = state.app;
  let events = eventIds.map(id => eventById[id]);
  events = events.sort((e1, e2) =>
    moment.unix(e1.date).isBefore(moment.unix(e2.date))
  );

  return {
    keyword,
    events: events,
    ...other
  };
};
export default connect(mapStateToProps)(withStyles(styles)(App));
