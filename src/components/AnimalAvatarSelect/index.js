import React, { PureComponent } from "react";
import { Select, MenuItem, withStyles } from "material-ui";
import AnimalAvatar from "../AnimalAvatar";

const styles = {
  avatarDescription: {
    display: "inline-block",
    marginLeft: 15
  },
  thumbnail: {
    width: 25,
    height: 25
  }
};

class AnimalAvatarSelect extends PureComponent {
  render() {
    const { classes, value, onChange } = this.props;
    return (
      <Select
        value={value}
        renderValue={selected => (
          <AnimalAvatar type={selected} className={classes.thumbnail} />
        )}
        onChange={event => onChange(event.target.value)}
      >
        <MenuItem value="bear">
          <AnimalAvatar type="bear" />
          <span className={classes.avatarDescription}>Bear</span>
        </MenuItem>
        <MenuItem value="cat">
          <AnimalAvatar type="cat" />
          <span className={classes.avatarDescription}>Cat</span>
        </MenuItem>
        <MenuItem value="dog">
          <AnimalAvatar type="dog" />
          <span className={classes.avatarDescription}>Dog</span>
        </MenuItem>
        <MenuItem value="fox">
          <AnimalAvatar type="fox" />
          <span className={classes.avatarDescription}>Fox</span>
        </MenuItem>
        <MenuItem value="horse">
          <AnimalAvatar type="horse" />
          <span className={classes.avatarDescription}>Horse</span>
        </MenuItem>
        <MenuItem value="owl">
          <AnimalAvatar type="owl" />
          <span className={classes.avatarDescription}>Owl</span>
        </MenuItem>
        <MenuItem value="penguin">
          <AnimalAvatar type="penguin" />
          <span className={classes.avatarDescription}>Penguin</span>
        </MenuItem>
        <MenuItem value="pig">
          <AnimalAvatar type="pig" />
          <span className={classes.avatarDescription}>Pig</span>
        </MenuItem>
      </Select>
    );
  }
}

export default withStyles(styles)(AnimalAvatarSelect);
