import React, { PureComponent } from "react";
import autobind from "class-autobind";
import {
  Button,
  Icon,
  Paper,
  MenuList,
  MenuItem,
  ListItemIcon,
  ListItemText,
  withStyles
} from "material-ui";
import Dropdown from "../Dropdown";

const styles = theme => ({
  moreButton: {
    minHeight: 0,
    minWidth: 0,
    padding: 0
  },
  menuItem: {
    fontSize: "0.875rem"
  }
});

class EventCardMenu extends PureComponent {
  constructor(props) {
    super(props);
    autobind(this);
  }

  onMenuClicked(event, open) {
    event.stopPropagation();
    open();
  }

  onMenuEditClicked(event, close) {
    event.stopPropagation();
    close();
    this.props.onEditClicked();
  }

  onMenuDeleteClicked(event, close) {
    event.stopPropagation();
    close();
    this.props.onDeleteClicked();
  }

  render() {
    const { classes } = this.props;
    return (
      <Dropdown
        placement="auto"
        renderButton={({ handleClick }) => (
          <Button
            className={classes.moreButton}
            onClick={e => this.onMenuClicked(e, handleClick)}
          >
            <Icon>more_vert</Icon>
          </Button>
        )}
        renderMenu={({ handleClose }) => (
          <Paper>
            <MenuList role="menu" style={{ textAlign: "left" }}>
              <MenuItem onClick={e => this.onMenuEditClicked(e, handleClose)}>
                <ListItemIcon>
                  <Icon>edit</Icon>
                </ListItemIcon>
                <ListItemText
                  disableTypography
                  className={classes.menuItem}
                  primary="Edit"
                />
              </MenuItem>
              <MenuItem onClick={e => this.onMenuDeleteClicked(e, handleClose)}>
                <ListItemIcon>
                  <Icon>delete</Icon>
                </ListItemIcon>
                <ListItemText
                  disableTypography
                  className={classes.menuItem}
                  primary="Delete"
                />
              </MenuItem>
            </MenuList>
          </Paper>
        )}
      />
    );
  }
}

export default withStyles(styles)(EventCardMenu);
