import React, { PureComponent } from "react";
import { Avatar } from "material-ui";

import bear from "./img/bear.svg";
import cat from "./img/cat.svg";
import dog from "./img/dog.svg";
import fox from "./img/fox.svg";
import horse from "./img/horse.svg";
import owl from "./img/owl.svg";
import penguin from "./img/penguin.svg";
import pig from "./img/pig.svg";

function getAnimaImage(type) {
  if (type === "bear") return bear;
  if (type === "cat") return cat;
  if (type === "dog") return dog;
  if (type === "fox") return fox;
  if (type === "horse") return horse;
  if (type === "owl") return owl;
  if (type === "penguin") return penguin;
  if (type === "pig") return pig;
  return bear;
}

export default class AnimaAvatar extends PureComponent {
  render() {
    const { type, ...rest } = this.props;
    return <Avatar src={getAnimaImage(type)} {...rest} />;
  }
}
