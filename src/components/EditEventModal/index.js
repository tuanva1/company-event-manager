import React, { PureComponent } from "react";
import autobind from "class-autobind";
import moment from "moment";
import ReactDOM from "react-dom";
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  TextField,
  FormGroup,
  InputLabel,
  Button,
  MenuItem,
  Select,
  withStyles
} from "material-ui";
import { TimePicker, DatePicker } from "material-ui-pickers";

const styles = theme => ({
  formGroup: {
    marginBottom: 25
  }
});

const initialForm = {
  id: undefined,
  title: "",
  date: moment().unix(),
  color: "#4CAF50",
  attendees: {}
};

class EditEvent extends PureComponent {
  state = initialForm;

  constructor(props) {
    super(props);
    autobind(this);

    if (props.event) {
      this.state = {
        id: props.event.id,
        title: props.event.title,
        date: props.event.date,
        color: props.event.color,
        attendees: props.event.attendees
      };
    }
  }

  onSubmit(event) {
    event.preventDefault();

    this.props.onSubmit(this.state);
    this.setState(initialForm);
  }

  onClose() {
    this.props.onClose();
    this.setState(initialForm);
  }

  renderModal() {
    const { classes, open, event } = this.props;
    const { title, date, color } = this.state;
    const momentDate = moment.unix(date);

    return (
      <Dialog open={open} onClose={this.onClose} fullWidth>
        <form onSubmit={this.onSubmit}>
          <DialogTitle>Event Modal</DialogTitle>
          <DialogContent>
            <FormGroup className={classes.formGroup}>
              <InputLabel shrink>Event title</InputLabel>
              <TextField
                value={title}
                required
                fullWidth
                onChange={e => this.setState({ title: e.target.value })}
                placeholder="Enter event title"
              />
            </FormGroup>

            <FormGroup className={classes.formGroup}>
              <InputLabel shrink>Start date</InputLabel>
              <DatePicker
                value={momentDate}
                fullWidth
                required
                onChange={date => this.setState({ date: date.unix() })}
                animateYearScrolling={false}
              />
            </FormGroup>

            <FormGroup className={classes.formGroup}>
              <InputLabel shrink>Start time</InputLabel>
              <TimePicker
                fullWidth
                required
                value={momentDate}
                onChange={date => this.setState({ date: date.unix() })}
              />
            </FormGroup>

            <FormGroup className={classes.formGroup}>
              <InputLabel shrink>Color</InputLabel>
              <Select
                value={color}
                onChange={event => this.setState({ color: event.target.value })}
              >
                <MenuItem
                  value="#4CAF50"
                  style={{ background: "#4CAF50", color: "#fff" }}
                >
                  Green
                </MenuItem>
                <MenuItem
                  value="#3F51B5"
                  style={{ background: "#3F51B5", color: "#fff" }}
                >
                  Blue
                </MenuItem>
                <MenuItem
                  value="#FF5722"
                  style={{ background: "#FF5722", color: "#fff" }}
                >
                  Red
                </MenuItem>
                <MenuItem
                  value="#9C27B0"
                  style={{ background: "#9C27B0", color: "#fff" }}
                >
                  Purple
                </MenuItem>
              </Select>
            </FormGroup>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.onClose}>Cancel</Button>
            <Button type="submit" color="primary">
              {!!event ? "Update" : "Add"}
            </Button>
          </DialogActions>
        </form>
      </Dialog>
    );
  }

  render() {
    return ReactDOM.createPortal(
      this.renderModal(),
      document.getElementById("modals")
    );
  }
}

export default withStyles(styles)(EditEvent);
