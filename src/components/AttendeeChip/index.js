import React, { PureComponent } from "react";
import { Chip, withStyles } from "material-ui";
import AnimalAvatar from "../AnimalAvatar";

const styles = theme => ({
  attendPeople: {
    margin: 5
  }
});

class AttendeeChip extends PureComponent {
  render() {
    const { attendee, onDelete, classes } = this.props;
    return (
      <Chip
        avatar={<AnimalAvatar type={attendee.avatar} />}
        className={classes.attendPeople}
        label={attendee.name}
        onDelete={onDelete}
      />
    );
  }
}

export default withStyles(styles)(AttendeeChip);
