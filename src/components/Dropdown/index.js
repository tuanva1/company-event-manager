import React, { PureComponent } from "react";
import classNames from "classnames";
import { Grow, ClickAwayListener, withStyles } from "material-ui";
import { Manager, Target, Popper } from "react-popper";

const styles = {
  root: {
    display: "inline-block"
  },
  popperClose: {
    pointerEvents: "none"
  }
};

class Dropdown extends PureComponent {
  state = {
    open: false
  };

  handleClick = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    const { classes, renderButton, renderMenu, placement } = this.props;
    const { open } = this.state;

    return (
      <div className={classes.root}>
        <Manager>
          <Target>
            {renderButton({
              isOpen: open,
              handleClick: this.handleClick
            })}
          </Target>
          <Popper
            placement={placement}
            eventsEnabled={open}
            style={{ zIndex: 1000 }}
            className={classNames({ [classes.popperClose]: !open })}
          >
            <ClickAwayListener onClickAway={this.handleClose}>
              <Grow
                in={open}
                id="menu-list"
                style={{ transformOrigin: "0 0 0" }}
              >
                {renderMenu({ handleClose: this.handleClose })}
              </Grow>
            </ClickAwayListener>
          </Popper>
        </Manager>
      </div>
    );
  }
}

export default withStyles(styles)(Dropdown);
