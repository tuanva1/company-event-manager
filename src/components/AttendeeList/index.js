import React, { PureComponent } from "react";
import { Typography, Button, Icon, withStyles } from "material-ui";
import AttendeeChip from "../AttendeeChip";

const styles = {
  wrapper: {
    display: "flex",
    flexWrap: "wrap"
  },
  title: {
    fontSize: "0.8rem",
    margin: 7
  },
  addBtn: {
    width: 29,
    height: 29,
    minHeight: 29,
    margin: 5
  }
};

class AttendeeList extends PureComponent {
  render() {
    const {
      classes,
      title,
      attendees,
      onDeleteItem,
      onAddButtonClicked
    } = this.props;

    return (
      <div>
        {title && (
          <Typography type="caption" className={classes.title}>
            {title}
          </Typography>
        )}
        <div className={classes.wrapper}>
          {attendees.map(a => (
            <AttendeeChip
              key={a.id}
              attendee={a}
              onDelete={() => onDeleteItem(a.id)}
            />
          ))}
          <Button
            className={classes.addBtn}
            fab
            color="secondary"
            onClick={onAddButtonClicked}
          >
            <Icon>add</Icon>
          </Button>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(AttendeeList);
