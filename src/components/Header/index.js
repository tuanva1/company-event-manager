import React, { PureComponent } from "react";
import { AppBar, Toolbar, Typography, withStyles } from "material-ui";

const styles = {
  flex: {
    flex: 1
  }
};

class Header extends PureComponent {
  render() {
    const { classes } = this.props;
    return (
      <AppBar position="static" color="primary">
        <Toolbar>
          <Typography type="title" color="inherit" className={classes.flex}>
            Pascalia Events
          </Typography>
          <Typography color="inherit">v1.2</Typography>
        </Toolbar>
      </AppBar>
    );
  }
}

export default withStyles(styles)(Header);
