import React, { PureComponent } from "react";
import autobind from "class-autobind";
import ReactDOM from "react-dom";
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  TextField,
  Button
} from "material-ui";
import AnimalAvatarSelect from "../AnimalAvatarSelect";

export default class AddAttendeeModal extends PureComponent {
  constructor(props) {
    super(props);
    autobind(this);
    this.state = {
      name: localStorage.getItem("lastAttendeeName") || "",
      avatar: localStorage.getItem("lastAttendeeAvatar") || "bear",
      willAttend: props.willAttend
    };
  }

  onSubmit(event) {
    event.preventDefault();
    this.props.onSubmit(this.state);
  }

  onClose() {
    this.props.onClose();
  }

  renderModal() {
    const { open } = this.props;
    const { name, avatar } = this.state;
    return (
      <Dialog open={open} onClose={this.onClose} fullWidth>
        <form onSubmit={this.onSubmit}>
          <DialogTitle>Add attendee</DialogTitle>
          <DialogContent style={{ display: "flex" }}>
            <AnimalAvatarSelect
              value={avatar}
              onChange={type => this.setState({ avatar: type })}
            />
            <TextField
              value={name}
              required
              onChange={e => this.setState({ name: e.target.value })}
              placeholder="Enter attendee name"
              style={{ flex: 1, marginLeft: 20 }}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.onClose}>Cancel</Button>
            <Button type="submit" color="primary">
              Add
            </Button>
          </DialogActions>
        </form>
      </Dialog>
    );
  }

  render() {
    return ReactDOM.createPortal(
      this.renderModal(),
      document.getElementById("modals")
    );
  }
}
