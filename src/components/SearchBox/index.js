import React, { PureComponent } from "react";
import {
  Switch,
  FormControl,
  FormControlLabel,
  Input,
  InputAdornment,
  IconButton,
  withStyles
} from "material-ui";
import algoliaLogo from "../../img/search-by-algolia.svg";

const styles = {
  wrapper: {
    display: "flex",
    justifyContent: "space-between",
    flexWrap: "wrap",
    flexDirection: "row-reverse",
    "@media (max-width: 400px)": {
      flexDirection: "column"
    }
  },
  logo: {
    height: 20,
    marginTop: 14
  }
};

class SearchBox extends PureComponent {
  render() {
    const {
      classes,
      keyword,
      onKeywordChange,
      showFinishedEvent,
      toggleShowFinishedEvent
    } = this.props;
    return (
      <div>
        <FormControl fullWidth>
          <Input
            placeholder="Enter event title"
            color="primary"
            value={keyword}
            onChange={e => onKeywordChange(e.target.value)}
            startAdornment={
              <InputAdornment position="start">
                <IconButton>search</IconButton>
              </InputAdornment>
            }
          />
        </FormControl>
        <div className={classes.wrapper}>
          <img className={classes.logo} alt="algolia-logo" src={algoliaLogo} />
          <FormControlLabel
            label="Show finished events"
            control={
              <Switch
                checked={showFinishedEvent}
                onChange={(e, checked) => toggleShowFinishedEvent(checked)}
              />
            }
          />
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(SearchBox);
