import React, { PureComponent } from "react";
import { Snackbar, IconButton } from "material-ui";
import { connect, actions } from "mirrorx";

class NotificationProvider extends PureComponent {
  render() {
    const { children, message } = this.props;
    return (
      <div>
        {children}
        <Snackbar
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "center"
          }}
          open={message !== ""}
          onClose={actions.notification.clearNotification}
          SnackbarContentProps={{
            "aria-describedby": "message-id"
          }}
          message={<span>{message}</span>}
          action={[
            <IconButton
              key="close"
              aria-label="Close"
              color="inherit"
              onClick={actions.notification.clearNotification}
            >
              close
            </IconButton>
          ]}
        />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return state.notification;
};

export default connect(mapStateToProps)(NotificationProvider);
