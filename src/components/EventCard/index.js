import React, { Fragment, Component } from "react";
import autobind from "class-autobind";
import moment from "moment";
import { actions } from "mirrorx";
import {
  ExpansionPanel,
  ExpansionPanelSummary,
  ExpansionPanelDetails,
  Typography,
  Hidden,
  Grid,
  withStyles
} from "material-ui";
import EventCardMenu from "../EventCardMenu";
import AddAttendeeModal from "../AddAttendeeModal";
import EditEventModal from "../EditEventModal";
import ConfirmModal from "../ConfirmModal";
import AttendeeList from "../AttendeeList";

const styles = theme => ({
  marker: {
    borderLeft: "8px solid"
  },
  eventContent: {
    borderTop: "1px solid rgba(0,0,0,0.12)",
    padding: "12px 14px",
    flexWrap: "wrap",
    flexDirection: "column"
  },
  inline: {
    display: "inline-block",
    fontStyle: "italic"
  }
});

class EventCard extends Component {
  state = {
    openAddWillAttendeeModal: false,
    openAddWillNotAttendeeModal: false,
    openEditEventModal: false,
    openConfirmModal: false
  };

  constructor(props) {
    super(props);
    autobind(this);
  }

  onEditEvent(event) {
    actions.app.saveEvent(event);
    this.onCloseModal("openEditEventModal");
  }

  onDeleteEvent() {
    actions.app.removeEvent(this.props.event.id);
    this.onCloseModal("openConfirmModal");
  }

  onOpenModal(modalName) {
    this.setState({
      [modalName]: true
    });
  }

  onCloseModal(modalName) {
    this.setState({
      [modalName]: false
    });
  }

  onAddAttendee(data, modalName) {
    actions.app.addAttendee({ eventId: this.props.event.id, ...data });
    this.onCloseModal(modalName);
  }

  render() {
    const { classes, event, defaultExpanded } = this.props;
    const {
      openAddWillAttendeeModal,
      openAddWillNotAttendeeModal,
      openEditEventModal,
      openConfirmModal
    } = this.state;
    const eventDate = moment.unix(event.date);
    const attendees = event.attendees ? Object.values(event.attendees) : [];
    attendees.sort((a1, a2) => a1.joinAt - a2.joinAt);
    const willAttends = attendees.filter(a => a.willAttend);
    const willNotAttends = attendees.filter(a => !a.willAttend);

    return (
      <Fragment>
        <ExpansionPanel defaultExpanded={defaultExpanded}>
          <ExpansionPanelSummary
            className={classes.marker}
            style={{ borderLeftColor: event.color }}
          >
            <Grid container style={{ paddingRight: 0 }}>
              <Grid item xs={4} sm={5} md={4}>
                <Typography className={classes.eventDate}>
                  {eventDate.format("DD/MM/YY")}{" "}
                  <Typography type="caption" className={classes.inline}>
                    ({eventDate.format("hh:mm A")})
                  </Typography>
                </Typography>
              </Grid>
              <Grid item xs={6} sm={5} md={4}>
                <Typography className={classes.eventTitle}>
                  {event.title}{" "}
                  <Hidden mdUp>
                    <Typography type="caption" className={classes.inline}>
                      ({willAttends.length} people joined)
                    </Typography>
                  </Hidden>
                </Typography>
              </Grid>
              <Hidden smDown>
                <Grid item md={2}>
                  <Typography>{willAttends.length} people</Typography>
                </Grid>
              </Hidden>
              <Grid item xs={2} style={{ textAlign: "right" }}>
                <EventCardMenu
                  onEditClicked={() => this.onOpenModal("openEditEventModal")}
                  onDeleteClicked={() => this.onOpenModal("openConfirmModal")}
                />
              </Grid>
            </Grid>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails className={classes.eventContent}>
            <AttendeeList
              title="Will Attend"
              attendees={willAttends}
              onDeleteItem={attendeeId =>
                actions.app.removeAttendee({
                  eventId: event.id,
                  attendeeId
                })
              }
              onAddButtonClicked={() =>
                this.onOpenModal("openAddWillAttendeeModal")
              }
            />
            <AttendeeList
              title="Will Not Attend"
              attendees={willNotAttends}
              onDeleteItem={attendeeId =>
                actions.app.removeAttendee({
                  eventId: event.id,
                  attendeeId
                })
              }
              onAddButtonClicked={() =>
                this.onOpenModal("openAddWillNotAttendeeModal")
              }
            />
          </ExpansionPanelDetails>
        </ExpansionPanel>
        {openAddWillAttendeeModal && (
          <AddAttendeeModal
            open={true}
            willAttend={true}
            onClose={() => this.onCloseModal("openAddWillAttendeeModal")}
            onSubmit={data =>
              this.onAddAttendee(data, "openAddWillAttendeeModal")
            }
          />
        )}
        {openAddWillNotAttendeeModal && (
          <AddAttendeeModal
            open={true}
            willAttend={false}
            onClose={() => this.onCloseModal("openAddWillNotAttendeeModal")}
            onSubmit={data =>
              this.onAddAttendee(data, "openAddWillNotAttendeeModal")
            }
          />
        )}
        {openEditEventModal && (
          <EditEventModal
            open={true}
            event={event}
            onClose={() => this.onCloseModal("openEditEventModal")}
            onSubmit={this.onEditEvent}
          />
        )}
        {openConfirmModal && (
          <ConfirmModal
            open={true}
            message="Do you want to delete this event? Delete action cannot be undone."
            onClose={() => this.onCloseModal("openConfirmModal")}
            onSubmit={this.onDeleteEvent}
          />
        )}
      </Fragment>
    );
  }
}

export default withStyles(styles)(EventCard);
