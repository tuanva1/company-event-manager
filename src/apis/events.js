import moment from "moment";
import firebase from "firebase/app";
import "firebase/firestore";
import algoliasearch from "algoliasearch";
import config from "./firebase_config.json";

firebase.initializeApp(config);
const firestore = firebase.firestore();
const collection = firestore.collection("events");

const algoliaClient = algoliasearch(
  process.env.REACT_APP_ALGOLIA_APP_ID,
  process.env.REACT_APP_ALGOLIA_API_KEY
);
const eventIndex = algoliaClient.initIndex("events");

export default {
  async getEvents() {
    const startOfDay = moment()
      .startOf("day")
      .unix();
    const result = await collection
      .where("date", ">=", startOfDay)
      .orderBy("date", "desc")
      .limit(20)
      .get();
    const events = [];
    result.forEach(doc => {
      events.push(doc.data());
    });
    return events;
  },

  async searchEvents(keyword = "", showFinishedEvent = false) {
    const query = { query: keyword, hitsPerPage: 20 };
    if (!showFinishedEvent) {
      const startOfDay = moment()
        .startOf("day")
        .unix();
      Object.assign(query, {
        filters: `date >= ${startOfDay}`
      });
    }
    const result = await eventIndex.search(query);
    return result.hits.map(e => ({
      id: e.id,
      title: e.title,
      date: e.date,
      color: e.color,
      attendees: e.attendees
    }));
  },

  async addAttendee(attendee) {
    const id = firestore.collection("attendees").doc().id;
    const newAttendee = {
      id,
      ...attendee,
      joinAt: moment().unix()
    };

    // Save last attendee information to localStorage
    localStorage.setItem("lastAttendeeName", newAttendee.name);
    localStorage.setItem("lastAttendeeAvatar", newAttendee.avatar);

    const updateData = {};
    updateData[`attendees.${id}`] = newAttendee;
    await collection.doc(attendee.eventId).update(updateData);
    return newAttendee;
  },

  async removeAttendee(eventId, attendeeId) {
    const updateData = {};
    updateData[
      `attendees.${attendeeId}`
    ] = firebase.firestore.FieldValue.delete();
    await collection.doc(eventId).update(updateData);
    return true;
  },

  async saveEvent(event) {
    const doc =
      event.id === undefined ? collection.doc() : collection.doc(event.id);
    const id = doc.id;

    await doc.set(
      {
        id,
        title: event.title,
        date: event.date,
        color: event.color
      },
      { merge: true }
    );

    event.id = id;
    return event;
  },

  async removeEvent(eventId) {
    await collection.doc(eventId).delete();
    return true;
  }
};
