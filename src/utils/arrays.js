export function isExist(array, item, compareFn) {
  for (let i = 0; i < array.length; i++) {
    if (compareFn(array[i], item)) {
      return true;
    }
  }
  return false;
}
