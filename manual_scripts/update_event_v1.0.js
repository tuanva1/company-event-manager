const firebase = require("firebase/app");
require("firebase/firestore");
const config = require("../src/apis/firebase_config.json");

firebase.initializeApp(config);
const firestore = firebase.firestore();
const collection = firestore.collection("events");
collection.get().then(result => {
  const events = result.docs.map(d => d.data());
  events.forEach(e => handleEvent(e));
});

function handleEvent(event) {
  const attendeeKeys = Object.keys(event.attendees);
  for (let i = 0; i < attendeeKeys.length; i++) {
    const key = attendeeKeys[i];
    const attendee = event.attendees[key];
    const avatar =
      attendee.name.toLowerCase() === "tuanva" ? "owl" : randomAvatar();
    event.attendees[key] = Object.assign({}, attendee, {
      willAttend: true,
      joinAt: event.date,
      avatar: avatar
    });
  }

  collection
    .doc(event.id)
    .set(event)
    .then(() => {
      console.log(`Finished update event ${event.id}`);
    });
}

function randomAvatar() {
  const availableAvatars = [
    "bear",
    "cat",
    "dog",
    "fox",
    "horse",
    "owl",
    "penguin",
    "pig"
  ];
  return availableAvatars[Math.floor(Math.random() * availableAvatars.length)];
}
